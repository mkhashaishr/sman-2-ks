import React from 'react';
import {Link} from 'react-router-dom';
import '../App.css';
import schoolLogo from './logo.svg';
import Headroom from 'react-headroom';


function Navbar() {

    return (
        <Headroom>
            <nav class="navbar navbar-expand-lg navbar-light shadow-sm" id="NavBar">
                <div class="container">
                    <Link class="navbar-brand" to="/">
                        <img src={schoolLogo} alt="logo" class="img-fluid"/>
                    </Link>
                    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse justify-content-end" id="navbarNav">
                        <ul class="navbar-nav">
                            <li class="nav-item">
                                <Link to="/" class="nav-link">Home</Link>
                            </li>
                            <li class="nav-item dropdown">
                                <Link class="nav-link dropdown-toggle" to="/" id="ProfileDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                    Profile
                                </Link>
                                <ul class="dropdown-menu dropdown-menu-light" aria-labelledby="ProfileDropdownMenuLink">
                                    <li><Link class="dropdown-item" to="/visi">Visi & Misi</Link></li>
                                    <li><Link class="dropdown-item" to="/sejarah">Sejarah Singkat</Link></li>
                                    <li><Link class="dropdown-item" to="/">Sarana Prasarana</Link></li>
                                    <li><Link class="dropdown-item" to="/">Identitas Sekolah</Link></li>
                                    <li><Link class="dropdown-item" to="/">Komite</Link></li>
                                    <li><Link class="dropdown-item" to="/">Program Kerja</Link></li>
                                    <li><Link class="dropdown-item" to="/">Kondisi Siswa</Link></li>
                                    <li><Link class="dropdown-item" to="/">Prestasi Sekolah</Link></li>
                                    <li><Link class="dropdown-item" to="/">IT Support</Link></li>
                                </ul>
                            </li>
                            <li class="nav-item">
                                <Link to="/" class="nav-link">Kesiswaan</Link>
                            </li>
                            <li class="nav-item dropdown">
                                <Link class="nav-link dropdown-toggle" to="/" id="KurikulumDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                    Kurikulum
                                </Link>
                                <ul class="dropdown-menu dropdown-menu-light" aria-labelledby="KurikulumDropdownMenuLink">
                                    <li><Link class="dropdown-item" to="/">Direktori Guru & TU</Link></li>
                                    <li><Link class="dropdown-item" to="/">Sejarah Singkat</Link></li>
                                    <li><Link class="dropdown-item" to="/">Silabus</Link></li>
                                    <li><Link class="dropdown-item" to="/">Materi Ajar</Link></li>
                                    <li><Link class="dropdown-item" to="/">Prestasi Guru</Link></li>
                                    <li><Link class="dropdown-item" to="/">Kalender Akademik</Link></li>
                                </ul>
                            </li>
                            <li class="nav-item dropdown">
                                <Link class="nav-link dropdown-toggle" to="/" id="DaftarUlangDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                    Daftar Ulang
                                </Link>
                                <ul class="dropdown-menu dropdown-menu-light" aria-labelledby="DaftarUlangDropdownMenuLink">
                                    <li><Link class="dropdown-item" to="/">Formulir Daftar Ulang</Link></li>
                                    <li><Link class="dropdown-item" to="/">SP Tata Tertib</Link></li>
                                    <li><Link class="dropdown-item" to="/">SP Tidak Menikah</Link></li>
                                    <li><Link class="dropdown-item" to="/">Angket Peminatan </Link></li>
                                    <li><Link class="dropdown-item" to="/">Form Ceklist Daftar Ulang</Link></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </Headroom>
    )
}

export default Navbar;