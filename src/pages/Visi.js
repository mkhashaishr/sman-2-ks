import React, { useEffect } from 'react';
import {Link} from 'react-router-dom';

function Visi() {
    useEffect(() => {
        document.title = "Visi & Misi - SMAN 2 Kakatu Steel Cilegon"
      }, [])
      
    return (
        <div className="visi">
            <div className="container overflow-hidden">
                <div className="row">
                    <div className="col-12">
                        <h1 className="judul">Visi & Misi</h1>
                        <hr />
                    </div>
                </div>
                <div className="row">
                    <div className="col-lg-9 mt-2">
                        <div className="visi--content px-3 py-5">
                            <h4 className="mb-xl-4">
                            Visi, Misi, dan Tujuan.
                            </h4>
                            <h5>Visi</h5>
                            <p>
                            “Unggul, berwawasan  lingkungan, dan menghasilkan lulusan yang Bertaqwa, Berprestasi, Berbudaya  serta berwawasan Global”
                            </p>
                            <h5>Misi</h5>
                            <p> 
                            Menerapkan pola manajemen partisipatif, transparan, dapat dipertanggungjawabkan dan memberikan layanan prima.
                            Mewujudkan sekolah yang peduli dan berbudaya lingkungan bagi semua warga sekolah dan masyarakat sekitar.
                            Meningkatkan sikap ketakwaan, sikap kepemimpinan dan disiplin melalui setiap kegiatan, baik intra maupun ekstrakurikuler yang berakar pada budaya bangsa.
                            Mengembangkan kemampuan akademik berwawasan global melalui penerapan dan pengembangan kurikulum nasional maupun internasional.
                            Menumbuhkan sikap berkompetisi yang positif dan sportif melalui kegiatan yang mengedepankan semangat kebangsaan.
                            Menciptakan dan menanamkan keteladanan perilaku melalui pengembangan budaya sekolah yang berlandaskan keagamaan , sosial masyarakat dan adiwiyata.
                            Membangun kegiatan kemitraan dalam pengembangan wawasan global .
                            Mengembangkan sikap enterpreneurship.
                            </p>
                            <h5>Tujuan</h5>
                            <p>
                            Menjadi sekolah terbaik yang berlandasan IMTAQ , IPTEK dan mampu bersaing di masyarakat Global.
                            Masuk seleksi olympiade tingkat Propinsi dan Nasional.
                            Lulus 100%.
                            Lulusan diterima di perguruan tinggi Negeri dan Swasta ternama 80% dari pendaftar.
                            Meraih prestasi kejuaraan di berbagai bidang akademik dan non akademik.
                            Meningkatkan profesionalisme guru melalui berbagai kegiatan workshop dan pendalaman materi.
                            Mempunyai sarana-prasarana pendukung kegiatan belajar mengajar yang memadai.
                            Memiliki manejemen sekolah yang akuntabel untuk mempertahankan standart ISO.
                            </p>
                        </div>
                    </div>
                    <div className="col-lg-3 mt-2 d-block">
                        <div className="sidebar">
                            <h5 class="fw-bold no-mt">Profile</h5>
                            <div>
                                <Link to="/visi" class="sidebar--link"><span class="d-block py-1 active">Visi & Misi</span></Link>
                                <Link to="/sejarah" class="sidebar--link"><span class="d-block py-1">Sejarah Singkat</span></Link>
                                <Link to="/" class="sidebar--link"><span class="d-block py-1">Sarana Prasarana</span></Link>
                                <Link to="/" class="sidebar--link"><span class="d-block py-1">Identitas Sekolah</span></Link>
                                <Link to="/" class="sidebar--link"><span class="d-block py-1">Komite</span></Link>
                                <Link to="/" class="sidebar--link"><span class="d-block py-1">Program Kerja</span></Link>
                                <Link to="/" class="sidebar--link"><span class="d-block py-1">Kondisi Siswa</span></Link>
                                <Link to="/" class="sidebar--link"><span class="d-block py-1">Prestasi Sekolah</span></Link>
                                <Link to="/" class="sidebar--link"><span class="d-block py-1">IT Support</span></Link>
                            </div>                               
                            <h5 class="fw-bold">Kurikulum</h5>
                            <div>
                                <Link to="/" class="sidebar--link"><span class="d-block py-1">Direktori Guru & TU</span></Link>
                                <Link to="/" class="sidebar--link"><span class="d-block py-1">Sejarah Singkat</span></Link>
                                <Link to="/" class="sidebar--link"><span class="d-block py-1">Silabus</span></Link>
                                <Link to="/" class="sidebar--link"><span class="d-block py-1">Materi Ajar</span></Link>
                                <Link to="/" class="sidebar--link"><span class="d-block py-1">Prestasi Guru</span></Link>
                                <Link to="/" class="sidebar--link"><span class="d-block py-1">Kalender Akademi</span></Link>
                            </div>
                            <h5 class="fw-bold">Daftar Ulang</h5>
                            <div>
                                <Link to="/" class="sidebar--link"><span class="d-block py-1">Formulir Daftar Ulang</span></Link>
                                <Link to="/" class="sidebar--link"><span class="d-block py-1">SP Tata Tertib</span></Link>
                                <Link to="/" class="sidebar--link"><span class="d-block py-1">SP Tidak Menikah</span></Link>
                                <Link to="/" class="sidebar--link"><span class="d-block py-1">Angket Peminatan </span></Link>
                                <Link to="/" class="sidebar--link"><span class="d-block py-1">Form Ceklist Daftar Ulang</span></Link>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Visi;