import React, {useEffect} from 'react';
import {Link} from 'react-router-dom';


function Sejarah() {
    useEffect(() => {
        document.title = "Sejarah Singkat - SMAN 2 Krakatu Steel Cilegon"
    }, [])

    return (
        <div className="sejarah">
            <div className="container overflow-hidden">
                <div className="row">
                    <div className="col-12">
                        <h1 className="judul">Sejarah Singkat</h1>
                        <hr />
                    </div>
                </div>
                <div className="row">
                    <div className="col-lg-9 mt-2">
                        <div className="sejarah--content px-3 py-5">
                            <h4 className="mb-xl-4">
                                Sejarah SMAN 2 KS Cilegon
                            </h4>
                            <p>
                                SMA NEGERI 2 KRAKATAU STEEL CILEGON pada mulanya bernama SMA YPWKS yang didirikan pada bulan Agustus tahun 1980. Pada saat itu SMA YPWKS belum memiliki 
                                gedung untuk tempat para siswanya belajar. Oleh karena itu para siswanya untuk sementara belajar di STM KS yang beralamat di Jl.Kotabumi No.1 Cilegon. 
                                Pada saat itu siswa SMA YPWKS  belajarnya siang hari karena pagi harinya dipakai oleh siswa STM KS. Tetapi sejak bulan Agustus tahun 1987 SMA YPWKS sudah 
                                memiliki gedung sendiri yang didirikan didaerah perumahan Komplek KS tepatnya di Jl. Semang Raya No.1 Cilegon. Gedung SMA YPWKS ini merupakan sumbangan 
                                dari YDPK PT. KS (Yayasan Dana Pensiun Karyawan PT.Krakatau Steel). Gedung ini resmi digunakan oleh SMA YPWKS pada tanggal 13 Agustus 1987, dan diresmikan 
                                oleh Direktur Utama PT. KS yaitu Ir. TUNGKY ARIWIBOWO dan Direktur Badan Pengurus YDPK KS yaitu Ir. KADARISMAN.
                            </p>
                            <p> 
                                Pada saat itu diwilayah Cilegon hanya memiliki satu sekolah yang negeri  yaitu SMA NEGERI 1 CILEGON. Oleh karena itu masyarakat Cilegon menginginkan ada sekolah 
                                negeri yang lain, maka SMA YPWKS berubah menjadi negeri. Pada tanggal 10 Juli 1996 SMA YPWKS resmi menjadi negeri. Sejak dinegerikan nama SMA YPWKS menjadi SMA
                                NEGERI 2 KRAKATAU STEEL CILEGON.
                            </p>
                        </div>
                    </div>
                    <div className="col-lg-3 mt-2 d-block">
                        <div className="sidebar">
                            <h5 class="fw-bold no-mt">Profile</h5>
                            <div>
                                <Link to="/visi" class="sidebar--link"><span class="d-block py-1">Visi & Misi</span></Link>
                                <Link to="/sejarah" class="sidebar--link"><span class="d-block py-1 active">Sejarah Singkat</span></Link>
                                <Link to="/" class="sidebar--link"><span class="d-block py-1">Sarana Prasarana</span></Link>
                                <Link to="/" class="sidebar--link"><span class="d-block py-1">Identitas Sekolah</span></Link>
                                <Link to="/" class="sidebar--link"><span class="d-block py-1">Komite</span></Link>
                                <Link to="/" class="sidebar--link"><span class="d-block py-1">Program Kerja</span></Link>
                                <Link to="/" class="sidebar--link"><span class="d-block py-1">Kondisi Siswa</span></Link>
                                <Link to="/" class="sidebar--link"><span class="d-block py-1">Prestasi Sekolah</span></Link>
                                <Link to="/" class="sidebar--link"><span class="d-block py-1">IT Support</span></Link>
                            </div>                               
                            <h5 class="fw-bold">Kurikulum</h5>
                            <div>
                                <Link to="/" class="sidebar--link"><span class="d-block py-1">Direktori Guru & TU</span></Link>
                                <Link to="/" class="sidebar--link"><span class="d-block py-1">Sejarah Singkat</span></Link>
                                <Link to="/" class="sidebar--link"><span class="d-block py-1">Silabus</span></Link>
                                <Link to="/" class="sidebar--link"><span class="d-block py-1">Materi Ajar</span></Link>
                                <Link to="/" class="sidebar--link"><span class="d-block py-1">Prestasi Guru</span></Link>
                                <Link to="/" class="sidebar--link"><span class="d-block py-1">Kalender Akademi</span></Link>
                            </div>
                            <h5 class="fw-bold">Daftar Ulang</h5>
                            <div>
                                <Link to="/" class="sidebar--link"><span class="d-block py-1">Formulir Daftar Ulang</span></Link>
                                <Link to="/" class="sidebar--link"><span class="d-block py-1">SP Tata Tertib</span></Link>
                                <Link to="/" class="sidebar--link"><span class="d-block py-1">SP Tidak Menikah</span></Link>
                                <Link to="/" class="sidebar--link"><span class="d-block py-1">Angket Peminatan </span></Link>
                                <Link to="/" class="sidebar--link"><span class="d-block py-1">Form Ceklist Daftar Ulang</span></Link>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Sejarah;