import React, {useEffect} from 'react';
import {Link} from 'react-router-dom';
import imgCarousel1 from '../inc/images/carousel/1.jpg';
import imgCarousel2 from '../inc/images/carousel/2.jpg';
import imgCarousel3 from '../inc/images/carousel/3.jpg';
import imageNews from '../inc/images/carousel/2.jpg';
import imageNews2 from '../inc/images/carousel/2.jpg';
import imageNews3 from '../inc/images/carousel/4.jpg';
import imageNews4 from '../inc/images/carousel/5.jpg';
import logoInsta from '../inc/images/socmed/logoInsta.png';
import imgInsta1 from '../inc/images/socmed/1.png';
import imgInsta2 from '../inc/images/socmed/2.png';
import imgInsta3 from '../inc/images/socmed/3.png';
import imgInsta4 from '../inc/images/socmed/4.png';
import imgInsta5 from '../inc/images/socmed/5.png';
import imgInsta6 from '../inc/images/socmed/6.png';
import imgInsta7 from '../inc/images/socmed/7.png';
import imgInsta8 from '../inc/images/socmed/8.png';


function Home() {
    useEffect(() => {
        document.title = "Home - SMAN 2 Kakatu Steel Cilegon"
      }, [])
    return (
        <div className="home">
            {/* Hero*/}
            <section className="hero">                                                                 
                <div id="carouselExampleLight" class="carousel carousel-light slide" data-bs-ride="carousel">
                    <div class="carousel-indicators">
                        <button type="button" data-bs-target="#carouselExampleLight" data-bs-slide-to="0" class="active rounded-circle" aria-current="true" aria-label="Slide 1"></button>
                        <button type="button" data-bs-target="#carouselExampleLight" data-bs-slide-to="1" class="rounded-circle" aria-label="Slide 2"></button>
                        <button type="button" data-bs-target="#carouselExampleLight" data-bs-slide-to="2" class="rounded-circle" aria-label="Slide 3"></button>
                    </div>
                    <div class="carousel-inner">
                        <div class="carousel-item active" data-bs-interval="10000">
                            <img src={imgCarousel1} class="d-block w-100" alt="..." />
                            <div className="overlay"></div>
                            <div class="carousel-caption d-none d-md-block">
                                <div className="caption">
                                    <h5>Penerimaan tahun ajaran 2021 / 2022</h5>
                                    <p className="my-3">
                                        Kementerian Pendidikan, Kebudayaan, Riset, & Teknologi menetapkan 
                                        delapan aturan baru terkait aturan PPDB 2021 melalui Peraturan Mendikbud
                                        No. 1 Tahun 2021. Aturan ini berlaku untuk jenjang SD hingga SMK.
                                    </p>
                                    <button className="btn btn-primary btn-lg text-white px-4 rounded-pill">Read More</button>
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item" data-bs-interval="2000">
                            <img src={imgCarousel2} class="d-block w-100" alt="..." />
                            <div className="overlay"></div>
                            <div class="carousel-caption d-none d-md-block">
                                <div className="caption">
                                    <h5>Info Awal Tahun Pelajaran 2021/2022</h5>
                                    <p className="my-3">
                                        Berdasarkan surat edaran Dinas Pendidikan dan Kebudayaan 
                                        Provinsi Banten Nomor 421.3/0037-Dindikbud/2021 tanggal
                                        9 Juli 2021, tentang pelaksanaan Masa Pengenalan Lingkungan Sekolah
                                    </p>
                                    <button className="btn btn-primary btn-lg text-white px-4 rounded-pill">Daftar Sekarang</button>
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img src={imgCarousel3} class="d-block w-100" alt="..." />
                            <div className="overlay"></div>
                            <div class="carousel-caption d-none d-md-block">
                                <div className="caption">
                                    <h5>Hasil Seleksi Jalur Zonasi</h5>
                                    <p className="my-3">
                                    Pengumuman hasil seleksi calon peserta didik baru
                                    SMA Negeri 2 Krakatau Steel Cilegon
                                    tahun pelajaran 2021 – 2022
                                    melalui jalur zonasi
                                    </p>
                                    <button className="btn btn-primary btn-lg text-white px-4 rounded-pill">Daftar Sekarang</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleLight" data-bs-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">Previous</span>
                    </button>
                    <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleLight" data-bs-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">Next</span>
                    </button>
                </div>                                 
            </section>

            {/* Kalender Pendidikan */}
            <section className="kalender">
                <div className="container">
                    <div className="row">
                        <h3 className="text-center">Kalender Pendidikan 2021/2022</h3>
                        <div className="heading-line"></div>
                    </div>
                    <div className="kalender--list my-5">
                        <div className="row">
                            <div className="col-md-4">
                                <h5>Juni 2021</h5>
                                <ul>
                                    <li>1: Libur Umum</li>
                                    <li>14-17: Penilaian Akhir Tahun</li>
                                    <li>25: Pembagian Buku Laporan</li>
                                    <li>28-30: Libur Semester</li>
                                </ul>
                            </div>
                            <div className="col-md-4 kalender--list--border">
                                <h5 className="fw-bold">Juli 2021</h5>
                                <ul>
                                    <li>1-11: Libur Semester</li>
                                    <li>13: Hari Pertama Sekolah</li>
                                    <li>13-15: Masa Pengenalan Lingkungan Sekolah</li>
                                    <li>31: Libur Umum</li>
                                </ul>
                            </div>
                            <div className="col-md-4">
                                <h5>Agustus 2021</h5>
                                <ul>
                                    <li>17: Libur Umum</li>
                                    <li>21: Libur Umum</li>
                                </ul>
                            </div>
                        </div>
                        <div className="row">
                            <Link to="/" className="text-primary text-center">
                                Lihat Detail 
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-arrow-right-short" viewBox="0 0 16 16">
                                    <path fill-rule="evenodd" d="M4 8a.5.5 0 0 1 .5-.5h5.793L8.146 5.354a.5.5 0 1 1 .708-.708l3 3a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708-.708L10.293 8.5H4.5A.5.5 0 0 1 4 8z"/>
                                </svg>
                            </Link>
                        </div>
                    </div>
                </div>
            </section>

            {/* News */}
            <section className="news">
                <div className="container">
                    <div className="row">
                        <h3 className="text-center">Latest News & Events</h3>
                        <div className="heading-line"></div>
                    </div>
                    <div className="news--list my-5">
                        <div className="row g-1">
                            <div className="col-md-6">
                                <div class="news--list--box">
                                    <img src={imageNews} alt="logo" class="img-fluid"/>
                                    <div class="news--list--box--before">
                                        <div class="caption">
                                            <h4>Info Awal Tahun Pelajaran 2021/2022</h4>
                                            <p>14 Juli 2021</p>
                                        </div>
                                    </div>
                                    <div class="news--list--box--after">
                                        <div class="caption">
                                            <h4>Info Awal Tahun Pelajaran 2021/2022</h4>
                                            <span>14 Juli 2021</span>
                                            <p className="my-3">Berdasarkan surat edaran Dinas Pendidikan dan Kebudayaan Provinsi Banten Nomor 421.3/0037-Dindikbud/2021 tanggal 9 Juli 2021, tentang pelaksanaan Masa Pengenalan Lingkungan Sekolah (MPLS) Tahun Pelajaran 2021 – 2022, maka dengan ini disampaikan bahwa :</p>
                                            <Link to="/visi">
                                                <button className="btn btn-primary rounded-pill px-4 py-2 text-white fw-bold">Read More</button>
                                            </Link>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-6">
                                <div class="news--list--box">
                                    <img src={imageNews2} alt="logo" class="img-fluid"/>
                                    <div class="news--list--box--before">
                                        <div class="caption">
                                            <h4>Pengumuman Belajar Dari Rumah (BDR)</h4>
                                            <p>9 Juli 2021</p>
                                        </div>
                                    </div>
                                    <div class="news--list--box--after">
                                        <div class="caption">
                                            <h4>Pengumuman Belajar Dari Rumah (BDR)</h4>
                                            <span>9 Juli 2021</span>
                                            <p className="my-3">Berdasarkan surat edaran Dinas Pendidikan dan Kebudayaan Provinsi Banten Nomor 421.3/0037-Dindikbud/2021 tanggal 9 Juli 2021, tentang pelaksanaan Masa Pengenalan Lingkungan Sekolah (MPLS) Tahun Pelajaran 2021 – 2022, maka dengan ini disampaikan bahwa :</p>
                                            <Link to="/visi">
                                                <button className="btn btn-primary rounded-pill px-4 py-2 text-white fw-bold">Read More</button>
                                            </Link>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-6">
                                <div class="news--list--box">
                                    <img src={imageNews3} alt="logo" class="img-fluid"/>
                                    <div class="news--list--box--before">
                                        <div class="caption">
                                            <h4>Hasil Seleksi Jalur Zonasi</h4>
                                            <p>1 Juli 2021</p>
                                        </div>
                                    </div>
                                    <div class="news--list--box--after">
                                        <div class="caption">
                                            <h4>Hasil Seleksi Jalur Zonasi</h4>
                                            <span>1 Juli 2021</span>
                                            <p className="my-3">Berdasarkan surat edaran Dinas Pendidikan dan Kebudayaan Provinsi Banten Nomor 421.3/0037-Dindikbud/2021 tanggal 9 Juli 2021, tentang pelaksanaan Masa Pengenalan Lingkungan Sekolah (MPLS) Tahun Pelajaran 2021 – 2022, maka dengan ini disampaikan bahwa :</p>
                                            <Link to="/visi">
                                                <button className="btn btn-primary rounded-pill px-4 py-2 text-white fw-bold">Read More</button>
                                            </Link>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-6">
                                <div class="news--list--box">
                                    <img src={imageNews4} alt="logo" class="img-fluid"/>
                                    <div class="news--list--box--before">
                                        <div class="caption">
                                            <h4>Triple A* Celebration</h4>
                                            <p>21 April 2021</p>
                                        </div>
                                    </div>
                                    <div class="news--list--box--after">
                                        <div class="caption">
                                            <h4>Triple A* Celebration</h4>
                                            <span>21 April 2021</span>
                                            <p className="my-3">Berdasarkan surat edaran Dinas Pendidikan dan Kebudayaan Provinsi Banten Nomor 421.3/0037-Dindikbud/2021 tanggal 9 Juli 2021, tentang pelaksanaan Masa Pengenalan Lingkungan Sekolah (MPLS) Tahun Pelajaran 2021 – 2022, maka dengan ini disampaikan bahwa :</p>
                                            <Link to="/visi">
                                                <button className="btn btn-primary rounded-pill px-4 py-2 text-white fw-bold">Read More</button>
                                            </Link>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="row text-center my-3">
                            <Link to="/" className="text-primary">Load More</Link>
                        </div>
                    </div>
                </div>
            </section>

            {/* Social */}
            <section className="social">
                <div className="container">
                    <div className="row">
                        <h3 className="text-center">Social</h3>
                        <div className="heading-line"></div>
                    </div>
                    <div className="social--feed mt-5">
                        <div className="row g-1">
                            <div className="col-md-6 col-lg-4 col-xl-3">
                                <div className="social--feed--item">
                                    <div class="card">
                                        <div className="logo-insta logo-insta d-flex m-2">
                                            <img src={logoInsta} alt="asasa" className="rounded-circle" width="30px" height="30px" />
                                            <span className="mx-2 mt-1">sman2ks</span>
                                        </div>
                                        <a href="https://www.instagram.com/sman2ks/" target="_blank" rel="noreferrer" className="stretched-link">
                                            <img src={imgInsta1} class="card-img-top img-fluid" alt="..." />
                                        </a>
                                        <div class="m-2">
                                        <h5>sman2ks</h5>
                                        <p>Back up and running! @rlc@tammyabraham @musonda @conorgallagher92 @yungchalobah #sman2ks #football</p>
                                        </div>
                                        <div class="card-footer bg-white d-flex">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-heart" viewBox="0 0 16 16">
                                                <path d="m8 2.748-.717-.737C5.6.281 2.514.878 1.4 3.053c-.523 1.023-.641 2.5.314 4.385.92 1.815 2.834 3.989 6.286 6.357 3.452-2.368 5.365-4.542 6.286-6.357.955-1.886.838-3.362.314-4.385C13.486.878 10.4.28 8.717 2.01L8 2.748zM8 15C-7.333 4.868 3.279-3.04 7.824 1.143c.06.055.119.112.176.171a3.12 3.12 0 0 1 .176-.17C12.72-3.042 23.333 4.867 8 15z"/>
                                            </svg>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-chat mx-2" viewBox="0 0 16 16">
                                                <path d="M2.678 11.894a1 1 0 0 1 .287.801 10.97 10.97 0 0 1-.398 2c1.395-.323 2.247-.697 2.634-.893a1 1 0 0 1 .71-.074A8.06 8.06 0 0 0 8 14c3.996 0 7-2.807 7-6 0-3.192-3.004-6-7-6S1 4.808 1 8c0 1.468.617 2.83 1.678 3.894zm-.493 3.905a21.682 21.682 0 0 1-.713.129c-.2.032-.352-.176-.273-.362a9.68 9.68 0 0 0 .244-.637l.003-.01c.248-.72.45-1.548.524-2.319C.743 11.37 0 9.76 0 8c0-3.866 3.582-7 8-7s8 3.134 8 7-3.582 7-8 7a9.06 9.06 0 0 1-2.347-.306c-.52.263-1.639.742-3.468 1.105z"/>
                                            </svg>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-cursor" viewBox="0 0 16 16">
                                                <path d="M14.082 2.182a.5.5 0 0 1 .103.557L8.528 15.467a.5.5 0 0 1-.917-.007L5.57 10.694.803 8.652a.5.5 0 0 1-.006-.916l12.728-5.657a.5.5 0 0 1 .556.103zM2.25 8.184l3.897 1.67a.5.5 0 0 1 .262.263l1.67 3.897L12.743 3.52 2.25 8.184z"/>
                                            </svg>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-bookmark ms-auto" viewBox="0 0 16 16">
                                                <path d="M2 2a2 2 0 0 1 2-2h8a2 2 0 0 1 2 2v13.5a.5.5 0 0 1-.777.416L8 13.101l-5.223 2.815A.5.5 0 0 1 2 15.5V2zm2-1a1 1 0 0 0-1 1v12.566l4.723-2.482a.5.5 0 0 1 .554 0L13 14.566V2a1 1 0 0 0-1-1H4z"/>
                                            </svg>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-6 col-lg-4 col-xl-3">
                                <div className="social--feed--item">
                                    <div class="card">
                                        <div className="logo-insta d-flex m-2">
                                            <img src={logoInsta} alt="asasa" className="rounded-circle" width="30px" height="30px" />
                                            <span className="mx-2 mt-1">sman2ks</span>
                                        </div>
                                        <a href="https://www.instagram.com/sman2ks/" target="_blank" rel="noreferrer" className="stretched-link">
                                            <img src={imgInsta2} class="card-img-top img-fluid" alt="..." />
                                        </a>
                                        <div class="m-2">
                                        <h5>sman2ks</h5>
                                        <p>Not just great players. Great people too. This group is special. It will come home eventually! We must believe  ❤️ #sman2ks #dbl #basketball</p>
                                        </div>
                                        <div class="card-footer bg-white d-flex">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-heart" viewBox="0 0 16 16">
                                                <path d="m8 2.748-.717-.737C5.6.281 2.514.878 1.4 3.053c-.523 1.023-.641 2.5.314 4.385.92 1.815 2.834 3.989 6.286 6.357 3.452-2.368 5.365-4.542 6.286-6.357.955-1.886.838-3.362.314-4.385C13.486.878 10.4.28 8.717 2.01L8 2.748zM8 15C-7.333 4.868 3.279-3.04 7.824 1.143c.06.055.119.112.176.171a3.12 3.12 0 0 1 .176-.17C12.72-3.042 23.333 4.867 8 15z"/>
                                            </svg>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-chat mx-2" viewBox="0 0 16 16">
                                                <path d="M2.678 11.894a1 1 0 0 1 .287.801 10.97 10.97 0 0 1-.398 2c1.395-.323 2.247-.697 2.634-.893a1 1 0 0 1 .71-.074A8.06 8.06 0 0 0 8 14c3.996 0 7-2.807 7-6 0-3.192-3.004-6-7-6S1 4.808 1 8c0 1.468.617 2.83 1.678 3.894zm-.493 3.905a21.682 21.682 0 0 1-.713.129c-.2.032-.352-.176-.273-.362a9.68 9.68 0 0 0 .244-.637l.003-.01c.248-.72.45-1.548.524-2.319C.743 11.37 0 9.76 0 8c0-3.866 3.582-7 8-7s8 3.134 8 7-3.582 7-8 7a9.06 9.06 0 0 1-2.347-.306c-.52.263-1.639.742-3.468 1.105z"/>
                                            </svg>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-cursor" viewBox="0 0 16 16">
                                                <path d="M14.082 2.182a.5.5 0 0 1 .103.557L8.528 15.467a.5.5 0 0 1-.917-.007L5.57 10.694.803 8.652a.5.5 0 0 1-.006-.916l12.728-5.657a.5.5 0 0 1 .556.103zM2.25 8.184l3.897 1.67a.5.5 0 0 1 .262.263l1.67 3.897L12.743 3.52 2.25 8.184z"/>
                                            </svg>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-bookmark ms-auto" viewBox="0 0 16 16">
                                                <path d="M2 2a2 2 0 0 1 2-2h8a2 2 0 0 1 2 2v13.5a.5.5 0 0 1-.777.416L8 13.101l-5.223 2.815A.5.5 0 0 1 2 15.5V2zm2-1a1 1 0 0 0-1 1v12.566l4.723-2.482a.5.5 0 0 1 .554 0L13 14.566V2a1 1 0 0 0-1-1H4z"/>
                                            </svg>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-6 col-lg-4 col-xl-3">
                                <div className="social--feed--item">
                                    <div class="card">
                                        <div className="logo-insta d-flex m-2">
                                            <img src={logoInsta} alt="asasa" className="rounded-circle" width="30px" height="30px" />
                                            <span className="mx-2 mt-1">sman2ks</span>
                                        </div>
                                        <a href="https://www.instagram.com/sman2ks/" target="_blank" rel="noreferrer" className="stretched-link">
                                            <img src={imgInsta3} class="card-img-top img-fluid" alt="..." />
                                        </a>
                                        <div class="m-2">
                                        <h5>sman2ks</h5>
                                        <p>when people ask what the recording process is like… 😂😂 seriously tho, I’m so thankful for all the people who brought it to life :) #sman2ks</p>
                                        </div>
                                        <div class="card-footer bg-white d-flex">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-heart" viewBox="0 0 16 16">
                                                <path d="m8 2.748-.717-.737C5.6.281 2.514.878 1.4 3.053c-.523 1.023-.641 2.5.314 4.385.92 1.815 2.834 3.989 6.286 6.357 3.452-2.368 5.365-4.542 6.286-6.357.955-1.886.838-3.362.314-4.385C13.486.878 10.4.28 8.717 2.01L8 2.748zM8 15C-7.333 4.868 3.279-3.04 7.824 1.143c.06.055.119.112.176.171a3.12 3.12 0 0 1 .176-.17C12.72-3.042 23.333 4.867 8 15z"/>
                                            </svg>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-chat mx-2" viewBox="0 0 16 16">
                                                <path d="M2.678 11.894a1 1 0 0 1 .287.801 10.97 10.97 0 0 1-.398 2c1.395-.323 2.247-.697 2.634-.893a1 1 0 0 1 .71-.074A8.06 8.06 0 0 0 8 14c3.996 0 7-2.807 7-6 0-3.192-3.004-6-7-6S1 4.808 1 8c0 1.468.617 2.83 1.678 3.894zm-.493 3.905a21.682 21.682 0 0 1-.713.129c-.2.032-.352-.176-.273-.362a9.68 9.68 0 0 0 .244-.637l.003-.01c.248-.72.45-1.548.524-2.319C.743 11.37 0 9.76 0 8c0-3.866 3.582-7 8-7s8 3.134 8 7-3.582 7-8 7a9.06 9.06 0 0 1-2.347-.306c-.52.263-1.639.742-3.468 1.105z"/>
                                            </svg>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-cursor" viewBox="0 0 16 16">
                                                <path d="M14.082 2.182a.5.5 0 0 1 .103.557L8.528 15.467a.5.5 0 0 1-.917-.007L5.57 10.694.803 8.652a.5.5 0 0 1-.006-.916l12.728-5.657a.5.5 0 0 1 .556.103zM2.25 8.184l3.897 1.67a.5.5 0 0 1 .262.263l1.67 3.897L12.743 3.52 2.25 8.184z"/>
                                            </svg>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-bookmark ms-auto" viewBox="0 0 16 16">
                                                <path d="M2 2a2 2 0 0 1 2-2h8a2 2 0 0 1 2 2v13.5a.5.5 0 0 1-.777.416L8 13.101l-5.223 2.815A.5.5 0 0 1 2 15.5V2zm2-1a1 1 0 0 0-1 1v12.566l4.723-2.482a.5.5 0 0 1 .554 0L13 14.566V2a1 1 0 0 0-1-1H4z"/>
                                            </svg>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-6 col-lg-4 col-xl-3">
                                <div className="social--feed--item">
                                    <div class="card">
                                        <div className="logo-insta d-flex m-2">
                                            <img src={logoInsta} alt="asasa" className="rounded-circle" width="30px" height="30px" />
                                            <span className="mx-2 mt-1">sman2ks</span>
                                        </div>
                                        <a href="https://www.instagram.com/sman2ks/" target="_blank" rel="noreferrer" className="stretched-link">
                                            <img src={imgInsta4} class="card-img-top img-fluid" alt="..." />
                                        </a>
                                        <div class="m-2">
                                        <h5>sman2ks</h5>
                                        <p>Good ending on a good week💙 well done boys #sman2ks #futsal</p>
                                        </div>
                                        <div class="card-footer bg-white d-flex">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-heart" viewBox="0 0 16 16">
                                                <path d="m8 2.748-.717-.737C5.6.281 2.514.878 1.4 3.053c-.523 1.023-.641 2.5.314 4.385.92 1.815 2.834 3.989 6.286 6.357 3.452-2.368 5.365-4.542 6.286-6.357.955-1.886.838-3.362.314-4.385C13.486.878 10.4.28 8.717 2.01L8 2.748zM8 15C-7.333 4.868 3.279-3.04 7.824 1.143c.06.055.119.112.176.171a3.12 3.12 0 0 1 .176-.17C12.72-3.042 23.333 4.867 8 15z"/>
                                            </svg>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-chat mx-2" viewBox="0 0 16 16">
                                                <path d="M2.678 11.894a1 1 0 0 1 .287.801 10.97 10.97 0 0 1-.398 2c1.395-.323 2.247-.697 2.634-.893a1 1 0 0 1 .71-.074A8.06 8.06 0 0 0 8 14c3.996 0 7-2.807 7-6 0-3.192-3.004-6-7-6S1 4.808 1 8c0 1.468.617 2.83 1.678 3.894zm-.493 3.905a21.682 21.682 0 0 1-.713.129c-.2.032-.352-.176-.273-.362a9.68 9.68 0 0 0 .244-.637l.003-.01c.248-.72.45-1.548.524-2.319C.743 11.37 0 9.76 0 8c0-3.866 3.582-7 8-7s8 3.134 8 7-3.582 7-8 7a9.06 9.06 0 0 1-2.347-.306c-.52.263-1.639.742-3.468 1.105z"/>
                                            </svg>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-cursor" viewBox="0 0 16 16">
                                                <path d="M14.082 2.182a.5.5 0 0 1 .103.557L8.528 15.467a.5.5 0 0 1-.917-.007L5.57 10.694.803 8.652a.5.5 0 0 1-.006-.916l12.728-5.657a.5.5 0 0 1 .556.103zM2.25 8.184l3.897 1.67a.5.5 0 0 1 .262.263l1.67 3.897L12.743 3.52 2.25 8.184z"/>
                                            </svg>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-bookmark ms-auto" viewBox="0 0 16 16">
                                                <path d="M2 2a2 2 0 0 1 2-2h8a2 2 0 0 1 2 2v13.5a.5.5 0 0 1-.777.416L8 13.101l-5.223 2.815A.5.5 0 0 1 2 15.5V2zm2-1a1 1 0 0 0-1 1v12.566l4.723-2.482a.5.5 0 0 1 .554 0L13 14.566V2a1 1 0 0 0-1-1H4z"/>
                                            </svg>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-6 col-lg-4 col-xl-3">
                                <div className="social--feed--item">
                                    <div class="card">
                                        <div className="logo-insta d-flex m-2">
                                            <img src={logoInsta} alt="asasa" className="rounded-circle" width="30px" height="30px" />
                                            <span className="mx-2 mt-1">sman2ks</span>
                                        </div>
                                        <a href="https://www.instagram.com/sman2ks/" target="_blank" rel="noreferrer" className="stretched-link">
                                            <img src={imgInsta5} class="card-img-top img-fluid" alt="..." />
                                        </a>
                                        <div class="m-2">
                                        <h5>sman2ks</h5>
                                        <p>Festival Gugak Indonesia ini bertujuan agar masyarakat  mengenal musik tradisional negara melalui kolaborasi yang epik.  </p>
                                        </div>
                                        <div class="card-footer bg-white d-flex">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-heart" viewBox="0 0 16 16">
                                                <path d="m8 2.748-.717-.737C5.6.281 2.514.878 1.4 3.053c-.523 1.023-.641 2.5.314 4.385.92 1.815 2.834 3.989 6.286 6.357 3.452-2.368 5.365-4.542 6.286-6.357.955-1.886.838-3.362.314-4.385C13.486.878 10.4.28 8.717 2.01L8 2.748zM8 15C-7.333 4.868 3.279-3.04 7.824 1.143c.06.055.119.112.176.171a3.12 3.12 0 0 1 .176-.17C12.72-3.042 23.333 4.867 8 15z"/>
                                            </svg>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-chat mx-2" viewBox="0 0 16 16">
                                                <path d="M2.678 11.894a1 1 0 0 1 .287.801 10.97 10.97 0 0 1-.398 2c1.395-.323 2.247-.697 2.634-.893a1 1 0 0 1 .71-.074A8.06 8.06 0 0 0 8 14c3.996 0 7-2.807 7-6 0-3.192-3.004-6-7-6S1 4.808 1 8c0 1.468.617 2.83 1.678 3.894zm-.493 3.905a21.682 21.682 0 0 1-.713.129c-.2.032-.352-.176-.273-.362a9.68 9.68 0 0 0 .244-.637l.003-.01c.248-.72.45-1.548.524-2.319C.743 11.37 0 9.76 0 8c0-3.866 3.582-7 8-7s8 3.134 8 7-3.582 7-8 7a9.06 9.06 0 0 1-2.347-.306c-.52.263-1.639.742-3.468 1.105z"/>
                                            </svg>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-cursor" viewBox="0 0 16 16">
                                                <path d="M14.082 2.182a.5.5 0 0 1 .103.557L8.528 15.467a.5.5 0 0 1-.917-.007L5.57 10.694.803 8.652a.5.5 0 0 1-.006-.916l12.728-5.657a.5.5 0 0 1 .556.103zM2.25 8.184l3.897 1.67a.5.5 0 0 1 .262.263l1.67 3.897L12.743 3.52 2.25 8.184z"/>
                                            </svg>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-bookmark ms-auto" viewBox="0 0 16 16">
                                                <path d="M2 2a2 2 0 0 1 2-2h8a2 2 0 0 1 2 2v13.5a.5.5 0 0 1-.777.416L8 13.101l-5.223 2.815A.5.5 0 0 1 2 15.5V2zm2-1a1 1 0 0 0-1 1v12.566l4.723-2.482a.5.5 0 0 1 .554 0L13 14.566V2a1 1 0 0 0-1-1H4z"/>
                                            </svg>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-6 col-lg-4 col-xl-3">
                                <div className="social--feed--item">
                                    <div class="card">
                                        <div className="logo-insta d-flex m-2">
                                            <img src={logoInsta} alt="asasa" className="rounded-circle" width="30px" height="30px" />
                                            <span className="mx-2 mt-1">sman2ks</span>
                                        </div>
                                        <a href="https://www.instagram.com/sman2ks/" target="_blank" rel="noreferrer" className="stretched-link">
                                            <img src={imgInsta6} class="card-img-top img-fluid" alt="..." />
                                        </a>
                                        <div class="m-2">
                                        <h5>sman2ks</h5>
                                        <p>Dirgahayu RI ke -75🇮🇩🇮🇩🇮🇩 Seribu orang tua hanya bisa bermimpi, tapi seorang pemuda mampu mengubah dunia.</p>
                                        </div>
                                        <div class="card-footer bg-white d-flex">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-heart" viewBox="0 0 16 16">
                                                <path d="m8 2.748-.717-.737C5.6.281 2.514.878 1.4 3.053c-.523 1.023-.641 2.5.314 4.385.92 1.815 2.834 3.989 6.286 6.357 3.452-2.368 5.365-4.542 6.286-6.357.955-1.886.838-3.362.314-4.385C13.486.878 10.4.28 8.717 2.01L8 2.748zM8 15C-7.333 4.868 3.279-3.04 7.824 1.143c.06.055.119.112.176.171a3.12 3.12 0 0 1 .176-.17C12.72-3.042 23.333 4.867 8 15z"/>
                                            </svg>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-chat mx-2" viewBox="0 0 16 16">
                                                <path d="M2.678 11.894a1 1 0 0 1 .287.801 10.97 10.97 0 0 1-.398 2c1.395-.323 2.247-.697 2.634-.893a1 1 0 0 1 .71-.074A8.06 8.06 0 0 0 8 14c3.996 0 7-2.807 7-6 0-3.192-3.004-6-7-6S1 4.808 1 8c0 1.468.617 2.83 1.678 3.894zm-.493 3.905a21.682 21.682 0 0 1-.713.129c-.2.032-.352-.176-.273-.362a9.68 9.68 0 0 0 .244-.637l.003-.01c.248-.72.45-1.548.524-2.319C.743 11.37 0 9.76 0 8c0-3.866 3.582-7 8-7s8 3.134 8 7-3.582 7-8 7a9.06 9.06 0 0 1-2.347-.306c-.52.263-1.639.742-3.468 1.105z"/>
                                            </svg>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-cursor" viewBox="0 0 16 16">
                                                <path d="M14.082 2.182a.5.5 0 0 1 .103.557L8.528 15.467a.5.5 0 0 1-.917-.007L5.57 10.694.803 8.652a.5.5 0 0 1-.006-.916l12.728-5.657a.5.5 0 0 1 .556.103zM2.25 8.184l3.897 1.67a.5.5 0 0 1 .262.263l1.67 3.897L12.743 3.52 2.25 8.184z"/>
                                            </svg>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-bookmark ms-auto" viewBox="0 0 16 16">
                                                <path d="M2 2a2 2 0 0 1 2-2h8a2 2 0 0 1 2 2v13.5a.5.5 0 0 1-.777.416L8 13.101l-5.223 2.815A.5.5 0 0 1 2 15.5V2zm2-1a1 1 0 0 0-1 1v12.566l4.723-2.482a.5.5 0 0 1 .554 0L13 14.566V2a1 1 0 0 0-1-1H4z"/>
                                            </svg>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-6 col-lg-4 col-xl-3">
                                <div className="social--feed--item">
                                    <div class="card">
                                            <div className="logo-insta d-flex m-2">
                                                <img src={logoInsta} alt="asasa" className="rounded-circle" width="30px" height="30px" />
                                                <span className="mx-2 mt-1">sman2ks</span>
                                            </div>
                                            <a href="https://www.instagram.com/sman2ks/" target="_blank" rel="noreferrer" className="stretched-link">
                                                <img src={imgInsta7} class="card-img-top img-fluid" alt="..." />
                                            </a>
                                            <div class="m-2">
                                            <h5>sman2ks</h5>
                                            <p>Last night’s defeat was a tough, and it’s taken me a while to process how gutted we all are to not lift that trophy. #sman2ks #basketball</p>
                                            </div>
                                            <div class="card-footer bg-white d-flex">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-heart" viewBox="0 0 16 16">
                                                    <path d="m8 2.748-.717-.737C5.6.281 2.514.878 1.4 3.053c-.523 1.023-.641 2.5.314 4.385.92 1.815 2.834 3.989 6.286 6.357 3.452-2.368 5.365-4.542 6.286-6.357.955-1.886.838-3.362.314-4.385C13.486.878 10.4.28 8.717 2.01L8 2.748zM8 15C-7.333 4.868 3.279-3.04 7.824 1.143c.06.055.119.112.176.171a3.12 3.12 0 0 1 .176-.17C12.72-3.042 23.333 4.867 8 15z"/>
                                                </svg>
                                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-chat mx-2" viewBox="0 0 16 16">
                                                    <path d="M2.678 11.894a1 1 0 0 1 .287.801 10.97 10.97 0 0 1-.398 2c1.395-.323 2.247-.697 2.634-.893a1 1 0 0 1 .71-.074A8.06 8.06 0 0 0 8 14c3.996 0 7-2.807 7-6 0-3.192-3.004-6-7-6S1 4.808 1 8c0 1.468.617 2.83 1.678 3.894zm-.493 3.905a21.682 21.682 0 0 1-.713.129c-.2.032-.352-.176-.273-.362a9.68 9.68 0 0 0 .244-.637l.003-.01c.248-.72.45-1.548.524-2.319C.743 11.37 0 9.76 0 8c0-3.866 3.582-7 8-7s8 3.134 8 7-3.582 7-8 7a9.06 9.06 0 0 1-2.347-.306c-.52.263-1.639.742-3.468 1.105z"/>
                                                </svg>
                                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-cursor" viewBox="0 0 16 16">
                                                    <path d="M14.082 2.182a.5.5 0 0 1 .103.557L8.528 15.467a.5.5 0 0 1-.917-.007L5.57 10.694.803 8.652a.5.5 0 0 1-.006-.916l12.728-5.657a.5.5 0 0 1 .556.103zM2.25 8.184l3.897 1.67a.5.5 0 0 1 .262.263l1.67 3.897L12.743 3.52 2.25 8.184z"/>
                                                </svg>
                                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-bookmark ms-auto" viewBox="0 0 16 16">
                                                    <path d="M2 2a2 2 0 0 1 2-2h8a2 2 0 0 1 2 2v13.5a.5.5 0 0 1-.777.416L8 13.101l-5.223 2.815A.5.5 0 0 1 2 15.5V2zm2-1a1 1 0 0 0-1 1v12.566l4.723-2.482a.5.5 0 0 1 .554 0L13 14.566V2a1 1 0 0 0-1-1H4z"/>
                                                </svg>
                                            </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-6 col-lg-4 col-xl-3">
                                <div className="social--feed--item">
                                    <div class="card">
                                        <div className="logo-insta d-flex m-2">
                                            <img src={logoInsta} alt="asasa" className="rounded-circle" width="30px" height="30px" />
                                            <span className="mx-2 mt-1">sman2ks</span>
                                        </div>
                                        <a href="https://www.instagram.com/sman2ks/" target="_blank" rel="noreferrer" className="stretched-link">
                                            <img src={imgInsta8} class="card-img-top img-fluid" alt="..." />
                                        </a>
                                        <div class="m-2">
                                        <h5>sman2ks</h5>
                                        <p>Siswa-siswi kelas XII lulusan tahun 2019/2020 melaksanakan penandatanganan ijazah di SMAN 3 Cilegon mengikuti Protokol Kesehatan.</p>
                                        </div>
                                        <div class="card-footer bg-white d-flex">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-heart" viewBox="0 0 16 16">
                                                <path d="m8 2.748-.717-.737C5.6.281 2.514.878 1.4 3.053c-.523 1.023-.641 2.5.314 4.385.92 1.815 2.834 3.989 6.286 6.357 3.452-2.368 5.365-4.542 6.286-6.357.955-1.886.838-3.362.314-4.385C13.486.878 10.4.28 8.717 2.01L8 2.748zM8 15C-7.333 4.868 3.279-3.04 7.824 1.143c.06.055.119.112.176.171a3.12 3.12 0 0 1 .176-.17C12.72-3.042 23.333 4.867 8 15z"/>
                                            </svg>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-chat mx-2" viewBox="0 0 16 16">
                                                <path d="M2.678 11.894a1 1 0 0 1 .287.801 10.97 10.97 0 0 1-.398 2c1.395-.323 2.247-.697 2.634-.893a1 1 0 0 1 .71-.074A8.06 8.06 0 0 0 8 14c3.996 0 7-2.807 7-6 0-3.192-3.004-6-7-6S1 4.808 1 8c0 1.468.617 2.83 1.678 3.894zm-.493 3.905a21.682 21.682 0 0 1-.713.129c-.2.032-.352-.176-.273-.362a9.68 9.68 0 0 0 .244-.637l.003-.01c.248-.72.45-1.548.524-2.319C.743 11.37 0 9.76 0 8c0-3.866 3.582-7 8-7s8 3.134 8 7-3.582 7-8 7a9.06 9.06 0 0 1-2.347-.306c-.52.263-1.639.742-3.468 1.105z"/>
                                            </svg>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-cursor" viewBox="0 0 16 16">
                                                <path d="M14.082 2.182a.5.5 0 0 1 .103.557L8.528 15.467a.5.5 0 0 1-.917-.007L5.57 10.694.803 8.652a.5.5 0 0 1-.006-.916l12.728-5.657a.5.5 0 0 1 .556.103zM2.25 8.184l3.897 1.67a.5.5 0 0 1 .262.263l1.67 3.897L12.743 3.52 2.25 8.184z"/>
                                            </svg>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-bookmark ms-auto" viewBox="0 0 16 16">
                                                <path d="M2 2a2 2 0 0 1 2-2h8a2 2 0 0 1 2 2v13.5a.5.5 0 0 1-.777.416L8 13.101l-5.223 2.815A.5.5 0 0 1 2 15.5V2zm2-1a1 1 0 0 0-1 1v12.566l4.723-2.482a.5.5 0 0 1 .554 0L13 14.566V2a1 1 0 0 0-1-1H4z"/>
                                            </svg>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row text-center my-3">
                        <Link to="/" className="text-primary">Load More</Link>
                    </div>
                </div>
            </section>    
        </div>
    )
}

export default Home;