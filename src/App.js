import React from "react";
import Navbar from "./inc/Navbar";
import Home from "./pages/Home";
import Visi from "./pages/Visi";
import Sejarah from "./pages/Sejarah";
import Footer from "./inc/Footer";
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import ScrollToTop from "./ScrollToTop";



function App() {
  return (
    <Router>
      <ScrollToTop />
      <div className="App">
        <Navbar />
        <Switch>
          <Route exact path="/">
            <Home/>
          </Route>
          <Route path="/visi">
            <Visi/>
          </Route>
          <Route path="/sejarah">
            <Sejarah />
          </Route>
        </Switch>
        <Footer />
      </div>
    </Router>
  );
}

export default App;
